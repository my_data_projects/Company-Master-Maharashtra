import read_data
import matplotlib.pyplot as plt


def data_classification_Auth_cap(file):
    data = read_data.read_data(file)
    classified_data = {'<= 1L': 0, '1L to 10L': 0, '10L to 1Cr': 0,
                       '1Cr to 10Cr': 0, '> 10Cr': 0}
    for row in data:
        try:
            if int(row['AUTHORIZED_CAP']) <= 100000:
                classified_data['<= 1L'] += 1
            elif 100000 < int(row['AUTHORIZED_CAP']) <= 1000000:
                classified_data['1L to 10L'] += 1
            elif 1000000 < int(row['AUTHORIZED_CAP']) <= 10000000:
                classified_data['10L to 1Cr'] += 1
            elif 10000000 < int(row['AUTHORIZED_CAP']) <= 100000000:
                classified_data['1Cr to 10Cr'] += 1
            else:
                classified_data['> 10Cr'] += 1
        except:
            classified_data['> 10Cr'] += 1
    return classified_data


def plot_hist(file):
    classified_data = data_classification_Auth_cap(file)
    plt.bar(list(classified_data.keys()), classified_data.values(), color='g')
    plt.show()


def execution_data_classification_Auth_cap(file):
    plot_hist(file)


file = 'Maharashtra.csv'
execution_data_classification_Auth_cap(file)
