import read_data
import matplotlib.pyplot as plt


def getting_districts_data(file):
    data = read_data.read_data(file)
    districts_info = {}
    for row in data:
        districts_info[row['District']] = row['Pin Code']
    return districts_info


def District_wise_Registrations(maharastra_csv, districts_csv):
    data = read_data.read_data(maharastra_csv)
    districts_info = getting_districts_data(districts_csv)
    pin_codes = []
    for row in data:
        if row['DATE_OF_REGISTRATION'][-2:] == '15':
            pin_codes.append(row['Registered_Office_Address'][-6:])
    Districts_registrations = {}
    for District, pin in districts_info.items():
        if str(pin) in pin_codes:
            Districts_registrations[District] = 0
    for District, pin in districts_info.items():
        if str(pin) in pin_codes:
            Districts_registrations[District] += pin_codes.count(str(pin))
    return Districts_registrations


def plot_bar(maharastra_csv, districts_csv):
    classified_data = District_wise_Registrations(
                                                maharastra_csv, districts_csv)
    plt.bar(list(classified_data.keys()), classified_data.values(), color='g')
    plt.gcf().autofmt_xdate()
    plt.show()


def execution_district_registrations(maharastra_csv, districts_csv):
    plot_bar(maharastra_csv, districts_csv)


maharastra_csv = 'Maharashtra.csv'
districts_csv = 'districts.csv'
execution_district_registrations(maharastra_csv, districts_csv)
