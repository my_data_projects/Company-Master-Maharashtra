import csv


def read_data(file):
    # Reading Data from csv
    with open(file, 'r', errors='ignore') as f:
        csv_reader = csv.DictReader(f)
        companies_list = list(csv_reader)
    return companies_list
