import read_data
from collections import Counter


def get_year_wise_top_Business_activities(file):
    data = read_data.read_data(file)
    last_10_years_data = {}
    for year in range(12, 22):
        last_10_years_data[str(year)] = []
    for row in data:
        if row['DATE_OF_REGISTRATION'][-2:] in list(last_10_years_data.keys()):
            val = row['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']
            last_10_years_data[row['DATE_OF_REGISTRATION'][-2:]].append(val)
    top_5_act = {}
    for year, business_activities in last_10_years_data.items():
        top_5_act[year] = dict(Counter(business_activities).most_common(5))
    return top_5_act


file = 'Maharashtra.csv'
print(get_year_wise_top_Business_activities(file))
