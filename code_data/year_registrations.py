import read_data
import matplotlib.pyplot as plt


def data_year_wise_registration(file):
    data = read_data.read_data(file)
    year_of_registraion = {}
    for row in data:
        year_of_registraion[row['DATE_OF_REGISTRATION'][-2:]] = 0

    for row in data:
        year_of_registraion[row['DATE_OF_REGISTRATION'][-2:]] += 1
    return year_of_registraion


def plot__bar(file):
    classified_data = data_year_wise_registration(file)
    plt.bar(list(classified_data.keys()), classified_data.values(), color='g')
    plt.gcf().autofmt_xdate()
    plt.show()


def execution_year_registrations(file):
    plot__bar(file)


file = 'Maharashtra.csv'
execution_year_registrations(file)
