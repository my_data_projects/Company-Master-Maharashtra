import csv
import matplotlib.pyplot as plt
from collections import Counter


def read_data(file):
    # Reading Data from csv
    with open(file, 'r', errors='ignore') as f:
        csv_reader = csv.DictReader(f)
        companies_list = list(csv_reader)
    return companies_list


def data_classification_Auth_cap(file):
    data = read_data(file)
    classified_data = {'<= 1L': 0, '1L to 10L': 0, '10L to 1Cr': 0,
                       '1Cr to 10Cr': 0, '> 10Cr': 0}
    for row in data:
        try:
            if int(row['AUTHORIZED_CAP']) <= 100000:
                classified_data['<= 1L'] += 1
            elif 100000 < int(row['AUTHORIZED_CAP']) <= 1000000:
                classified_data['1L to 10L'] += 1
            elif 1000000 < int(row['AUTHORIZED_CAP']) <= 10000000:
                classified_data['10L to 1Cr'] += 1
            elif 10000000 < int(row['AUTHORIZED_CAP']) <= 100000000:
                classified_data['1Cr to 10Cr'] += 1
            else:
                classified_data['> 10Cr'] += 1
        except:
            classified_data['> 10Cr'] += 1
    return classified_data


def plot_hist(file):
    classified_data = data_classification_Auth_cap(file)
    plt.bar(list(classified_data.keys()), classified_data.values(), color='g')
    plt.show()


def execution_data_classification_Auth_cap(file):
    plot_hist(file)


###############################################

def data_year_wise_registration(file):
    data = read_data(file)
    year_of_registraion = {}
    for row in data:
        year_of_registraion[row['DATE_OF_REGISTRATION'][-2:]] = 0

    for row in data:
        year_of_registraion[row['DATE_OF_REGISTRATION'][-2:]] += 1
    return year_of_registraion


def plot__bar(file):
    classified_data = data_year_wise_registration(file)
    plt.bar(list(classified_data.keys()), classified_data.values(), color='g')
    plt.gcf().autofmt_xdate()
    plt.show()


def execution_year_registrations(file):
    plot__bar(file)


###############################################


def getting_districts_data(file):
    data = read_data(file)
    districts_info = {}
    for row in data:
        districts_info[row['District']] = row['Pin Code']
    return districts_info


def District_wise_Registrations(maharastra_csv, districts_csv):
    data = read_data(maharastra_csv)
    districts_info = getting_districts_data(districts_csv)
    pin_codes = []
    for row in data:
        if row['DATE_OF_REGISTRATION'][-2:] == '15':
            pin_codes.append(row['Registered_Office_Address'][-6:])
    Districts_registrations = {}
    for District, pin in districts_info.items():
        if str(pin) in pin_codes:
            Districts_registrations[District] = 0
    for District, pin in districts_info.items():
        if str(pin) in pin_codes:
            Districts_registrations[District] += pin_codes.count(str(pin))
    return Districts_registrations


def plot_bar(maharastra_csv, districts_csv):
    classified_data = District_wise_Registrations(
                                                maharastra_csv, districts_csv)
    plt.bar(list(classified_data.keys()), classified_data.values(), color='g')
    plt.gcf().autofmt_xdate()
    plt.show()


def execution_district_registrations(maharastra_csv, districts_csv):
    plot_bar(maharastra_csv, districts_csv)


############################################################

def get_year_wise_top_Business_activities(file):
    data = read_data(file)
    last_10_years_data = {}
    for year in range(12, 22):
        last_10_years_data[str(year)] = []
    for row in data:
        if row['DATE_OF_REGISTRATION'][-2:] in list(last_10_years_data.keys()):
            val = row['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']
            last_10_years_data[row['DATE_OF_REGISTRATION'][-2:]].append(val)
    top_5_act = {}
    for year, business_activities in last_10_years_data.items():
        top_5_act[year] = dict(Counter(business_activities).most_common(5))
    return top_5_act


############################################################

def main():
    Maharashtra_csv = 'Maharashtra.csv'
    districts_csv = 'districts.csv'
    execution_data_classification_Auth_cap(Maharashtra_csv)
    execution_year_registrations(Maharashtra_csv)
    execution_district_registrations(Maharashtra_csv, districts_csv)

    
main()
